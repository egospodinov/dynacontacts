(function(){
	'use-strict';

	angular.module('dynacontacts').
		directive('contactImage',function($http){
			function getImage(name,callback){
				var url = 'https://ajax.googleapis.com/ajax/services/search/images?v=1.0&q='+name+'&callback=JSON_CALLBACK';
				$http.jsonp(url)
					.success(function(d){
						var url = d.responseData.results[0].url;
						callback(url);
					})	
			}

			return{
				restrict:"A",
				scope:{
					contact:"@contact"
				},
				link:function($scope, $elem, $attr){
					$scope.$watch('contact',function(contact){
						var contact = JSON.parse($scope.contact);
						if(contact.hasOwnProperty('firstName')){
							var name = contact.firstName + ' ' + contact.familyName;
							console.log(contact);
							getImage(name,function(url){
								$elem.attr('src',url);
							})
						}
					})
				}
			}
		})
})()