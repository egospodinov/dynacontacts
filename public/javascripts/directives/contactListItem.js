(function(){
	'use-strict';

	angular.module('dynacontacts')
		.directive('contactListItem',function(){
			return{
				restrict:"A",
				link:function($scope,$elem,$attr){
					$elem.on('click',function(e){
						var options = $(this).parent().find('.options');
						$('.contact-list-item .options').slideUp('fast');
						if(options.is(':visible')){
							return;
						}
						options.slideDown('fast');

					})
				}
			}
		})
})()