(function(){
	'use strict';

	angular.module('dynacontacts')
		.directive('bodyClick',function(){
			return {
				restrict:'A',
				link:function($scope,$elem,$attr){
					
					var menu = $('.navbar-collapse.collapse')
					$('body').on('click',function(ev){
						if(menu.hasClass('in')){
							menu.removeClass('in').attr('aria-expanded','false')
						}
					})
				}
			}
		});
})()