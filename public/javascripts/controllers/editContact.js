(function(){
	'use strict';

	angular.module('dynacontacts')
		.controller('editContact', function($scope, $location, $routeParams, contactService){

			//define the contact object
			$scope.contact = {};

			//get the contact id from the route params
			var id = $routeParams.id;
			console.log(id)
			contactService.getContact(id)
				.then(function(contact){
					$scope.contact = contact;
				})
			
			//when form is submitted
			$scope.submit = function(){
					if($scope.form.$invalid){
						return;
					}

					contactService.updateContact(id,$scope.contact).then(function(){
						$location.path("/list/"+id);
					});
			}

		})
})()