(function(){
	'use strict';

	 angular.module('dynacontacts')
	 	.controller('contactList',function($scope,contactService){
	 		
	 		//define the contacts
	 		$scope.contacts = {};

	 		//call getContacts method from contactService to get all the contacts
	 		contactService.getContacts().then(function(contacts){
	 			$scope.contacts = contacts;
	 		});

			
			$scope.search = function(){
				contactService.searchContact($scope.searchParam)
					.then(function(contacts){
						console.log(contacts);
						$scope.contacts = contacts;
					});
			}	 		

	 	});
})()