(function(){
	'use-strict';

	 angular.module('dynacontacts')
	 	.controller('contactDetail',function($scope, $location, $routeParams, contactService){
	 		
	 		$scope.contact = {};

	 		var contactId = $routeParams.id;
	 		contactService.getContact(contactId)
	 			.then(function(contact){
	 				$scope.contact = contact;
	 				$scope.contactFullName = contact.firstName+' '+contact.familyName;
	 			});


			$scope.delete = function(){
				contactService.deleteContact(contactId)
					.then(function(){
						$location.path('/list');
					});
			}
	 	});
})()