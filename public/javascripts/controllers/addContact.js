(function(){
	'use strict';

	angular.module('dynacontacts')
			.controller('addContact',function($scope,$location,contactService){

				$scope.contact = {};
					
				$scope.submit = function(){
					if($scope.form.$invalid){
						return;
					}

					contactService.addContact($scope.contact).then(function(){
						$location.path("/list");
					});
				}

			});
})()