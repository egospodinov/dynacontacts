(function(){
	'use strict';

	angular.module('dynacontacts',['ngRoute'])
		.config(function($routeProvider){
			$routeProvider
			.when('/list',{
				templateUrl:'./javascripts/templates/contactList.html',
				controller:'contactList'
			})
			.when('/list/:id',{
				templateUrl:'./javascripts/templates/contactDetail.html',
				controller:"contactDetail"
			})
			.when('/add',{
				templateUrl:'./javascripts/templates/contactAdd.html',
				controller:'addContact'
			})
			.when('/edit/:id',{
				templateUrl:'./javascripts/templates/contactAdd.html',
				controller:'editContact'
			})
			.otherwise('/list')
		});
})()