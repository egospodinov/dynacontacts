(function(){
	'use strict';

	angular.module('dynacontacts')
		.service('contactService',function($q, $http, $location){
			
			var MAIN_URL = $location.$$absUrl.split("#")[0];
			return {
				getContacts:function(){
					var deferred = $q.defer();
					$http.get(MAIN_URL+'contacts')
						.success(function(data){
							deferred.resolve(data);
						})
						.error(function(err){
							deferred.reject(err);
						});

					return deferred.promise;
				},
				searchContact:function(searchParam){
					var deferred = $q.defer();
					$http.post(MAIN_URL+'contacts/search',{searchParam:searchParam})
						.success(function(d){
							deferred.resolve(d);
						})
						.error(function(err){
							deferred.reject(err);
						});

						return deferred.promise;
				},
				getContact:function(id){
					var deferred = $q.defer();
					$http.get(MAIN_URL+'contacts/'+id)
						.success(function(contact){
							deferred.resolve(contact);
						})
						.error(function(err){
							deferred.reject(err);
						})

					return deferred.promise;
				},
				addContact:function(contact){
					var deferred = $q.defer();
					$http.post(MAIN_URL+'contacts',contact)
						.success(function(data){
							deferred.resolve(data);
						})
						.error(function(err){
							deferred.reject(err);
						})

						return deferred.promise;
				},
				updateContact:function(id,contact){
					var deferred = $q.defer();
					$http.put(MAIN_URL+'contacts/'+id,contact)
						.success(function(contact){
							deferred.resolve(contact);
						})
						.error(function(err){
							deferred.resolve(err);
						})

						return deferred.promise;
				},
				deleteContact:function(id){
					var deferred = $q.defer();
					$http.delete(MAIN_URL+'contacts/'+id)
						.success(function(data){
							deferred.resolve();
						})
						.error(function(err){
							deferred.reject();
						})

					return deferred.promise;
				}
			}
		})
})()