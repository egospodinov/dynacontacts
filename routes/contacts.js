var express = require('express');
var router = express.Router();
var model = require('../models/contactModel');


router.get('/',function(req, res){
	var contacts = model.getContacts();
	res.json(contacts);
});

router.post('/',function(req,res){
	var contact = model.createContact(req.body);
	res.json(contact);
});

router.get('/:id',function(req,res){
	var contact =  model.getContact(req.params.id);
	res.json(contact);
});

router.put('/:id',function(req,res){
	var contact= model.updateContact(req.params.id,req.body);
	res.json(contact);
});

router.delete('/:id',function(req,res){
	var success  = model.deleteContact(req.params.id);
	res.json({success:success})
});

router.post('/search',function(req,res){
	var contacts = model.search(req.body.searchParam);
	res.json(contacts);
});

module.exports = router;
