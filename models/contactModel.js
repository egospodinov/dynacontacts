var sha = require('sha1');
var fs = require('fs');

var ContactModel = function(){
	this.data;

	//searches contacts by first name
	this.search = function(searchParam){
		var obj  =  {};
		var data = this.data;
		var param = new RegExp(searchParam,'i');
		for(var i in data){
			if(data[i].firstName.match(param)){
				obj[i] = data[i]
			}
		}
		return obj;

	}

	//creates id for the contact entry
	this.createId = function(firstName,phoneNumber){
		var rand = Math.ceil(Math.random()*10000);
		var id = sha(firstName+rand+phoneNumber);
		return id;
	}

	//adds {contactData} object to the this.data object
	this.createContact = function(contactData){
		var id = this.createId(contactData.firstName,contactData.phoneNumber);
		contactData.id = id;
		this.data[id] = contactData;
		this.saveData();
		return this.data[id];
	}


	//returns this.data which is containing all the contacts
	this.getContacts = function(){
		var arr = [];
		for(var i in this.data){
			arr.push(this.data[i]);
		}
		return arr;
	}

	//gets contact id as parameter and returns the contact whith this id
	this.getContact = function(contactId){
		return this.data[contactId];
	}

	//updates a given contact
	this.updateContact = function(contactId,contactData){
		var contact = this.data[contactId];
		console.log(contactId);
		if(!contact){
			return false;
		}else{
			if(contactData){
				this.data[contactId] = contactData;
			}
			this.saveData();
			return this.data[contactId];
		}
	}

	//deletes contact
	this.deleteContact = function(contactId){
		
		if(!this.data[contactId]){
			return false;
		}else{
			delete this.data[contactId];
			return true;
		}
	}

	//saves the data in database.json
	this.saveData = function(){
	   fs.writeFileSync('./database/database.json',JSON.stringify(this.data));
	}

	// 'class' constructor which is reading the database.json file and parses it as json 
	this.init = function(){
		var data = fs.readFileSync('./database/database.json','UTF-8');
		this.data = JSON.parse(data);		
	}

	this.init();
}

//exports the 'class' with common.js pattern
module.exports = new ContactModel();